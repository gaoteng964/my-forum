package com.example.myforum.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myforum.R
import com.example.myforum.data.Community

class AdaperCommunity : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    val currentCommunityList = arrayListOf<Community>()

    inner class CommunityViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val tvCommunityTitle: TextView = view.findViewById(R.id.tvCommunityTitle)
        val tvCommunityContent: TextView = view.findViewById(R.id.tvCommunityContent)
        val tvCommunityAuthor: TextView = view.findViewById(R.id.tvCommunityAuthor)

        fun bindData(community: Community) {
            tvCommunityTitle.text = community.communityName
            tvCommunityContent.text = community.communityDescription
            tvCommunityAuthor.text = community.author
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return LayoutInflater.from(parent.context).inflate(R.layout.recycle_item_community, parent, false).run {
            CommunityViewHolder(this)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CommunityViewHolder).bindData(currentCommunityList[position])
    }

    override fun getItemCount(): Int = currentCommunityList.size

    fun setCurrentCommunities(communityList: List<Community>){
        currentCommunityList.clear()
        currentCommunityList.addAll(communityList)
        notifyDataSetChanged()
    }
}