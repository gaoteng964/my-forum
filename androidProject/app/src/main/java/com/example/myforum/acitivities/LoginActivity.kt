package com.example.myforum.acitivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.myforum.MainActivity
import com.example.myforum.R
import com.example.myforum.data.RegisterUser
import com.example.myforum.http.HttpConnection
import com.example.myforum.preference.MySharedPreference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber

class LoginActivity : AppCompatActivity() {

    private val loginFlow: MutableStateFlow<RegisterUser?> = MutableStateFlow(null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        registerLoginClickEvent()
        registerRegisterClickEvent()
        initLoginFlow()
    }

    private fun registerLoginClickEvent() {
        findViewById<Button>(R.id.btnLogin).setOnClickListener {
            val name = findViewById<EditText>(R.id.etName).text.toString()
            val password = findViewById<EditText>(R.id.etPassword).text.toString()
            if(name.isNullOrBlank()){
                Toast.makeText(this@LoginActivity.baseContext, R.string.login_activity_name_cant_be_blank_or_empty, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if(password.isNullOrBlank()){
                Toast.makeText(this@LoginActivity.baseContext, R.string.login_activity_password_cant_be_blank_or_empty, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            loginFlow.tryEmit(RegisterUser(name = name, password = password, age = 0))
        }
    }

    private fun initLoginFlow() {
        loginFlow.map {
            it ?: return@map false
            val isSuccess =  HttpConnection.login(it)
            if(isSuccess){
                MySharedPreference.saveUserInfo(it)
            }
            isSuccess
        }.flowOn(Dispatchers.IO).onEach {
            if(it){
                Intent(this@LoginActivity, MainActivity::class.java).run {
                    startActivity(this)
                }
            }else {
                Timber.i( " login fail and start to show info" )
                Toast.makeText(this@LoginActivity.baseContext, R.string.fail_to_login, Toast.LENGTH_SHORT).show()
            }
        }.flowOn(Dispatchers.Main).launchIn(lifecycleScope)
    }

    private fun registerRegisterClickEvent() {
        findViewById<Button>(R.id.btnRegister).setOnClickListener {
            Intent(this@LoginActivity, RegisterActivity::class.java).run {
                startActivity(this)
            }
        }
    }

}