package com.example.myforum.acitivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.example.myforum.MainActivity
import com.example.myforum.R
import com.example.myforum.preference.MySharedPreference
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        lifecycleScope.launch {
            delay(2000)
            val currentUserInfo = MySharedPreference.getUserInfo()
            if(currentUserInfo != null){
                Intent(this@SplashActivity, MainActivity::class.java).run {
                    startActivity(this)
                }
            } else{
                Intent(this@SplashActivity, LoginActivity::class.java).run {
                    startActivity(this)
                }
            }
        }
    }
}