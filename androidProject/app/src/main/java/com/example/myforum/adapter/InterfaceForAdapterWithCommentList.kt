package com.example.myforum.adapter

import com.example.myforum.data.Comment

interface InterfaceForAdapterWithCommentList {
    fun getCommentList(): List<Comment>
}