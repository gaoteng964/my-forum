package com.example.myforum.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myforum.R
import com.example.myforum.data.Comment

class AdapterChildComment():  RecyclerView.Adapter<RecyclerView.ViewHolder>(), InterfaceForAdapterWithCommentList  {

    private val currentComments: ArrayList<Comment> = arrayListOf()

    private lateinit var thumbCommentCallBack: (Comment) -> Unit

    inner class CommentViewHolder(private val view: View):RecyclerView.ViewHolder(view)  {

        val tvCommentAuthor =  view.findViewById<TextView>(R.id.tvCommentAuthor)
        val tvThumcount = view.findViewById<TextView>(R.id.tvThumcount)
        val tvCommentContent = view.findViewById<TextView>(R.id.tvCommentContent)
        val ivThumbcount = view.findViewById<ImageView>(R.id.ivThumbcount)

        fun bindData(comment: Comment){
            tvCommentContent.text = comment.content
            tvCommentAuthor.text = comment.author
            tvThumcount.text = comment.thumbCount.toString()
            ivThumbcount.setOnClickListener {
                thumbCommentCallBack.invoke(comment)
            }
            tvThumcount.setOnClickListener {
                thumbCommentCallBack.invoke(comment)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return LayoutInflater.from(parent.context).inflate(R.layout.child_comment_recycleview_item, parent, false).run {
            CommentViewHolder(this)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CommentViewHolder).bindData(currentComments[position])
    }

    override fun getItemCount(): Int = currentComments.size

    fun  setThumbCommentCallBack(callback: (Comment) -> Unit) {
        thumbCommentCallBack = callback
    }

    override fun getCommentList(): List<Comment> {
        return currentComments
    }

    fun setCommentList(comments: List<Comment>){
        currentComments.clear()
        currentComments.addAll(comments)
        notifyDataSetChanged()
    }
}