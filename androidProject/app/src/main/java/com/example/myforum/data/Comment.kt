package com.example.myforum.data

import android.os.Parcel
import android.os.Parcelable

data class Comment(
    val id: Int? = null,
    var parentId: Int = -1,
    val author: String,
    val title: String,
    val content: String,
    val community: String,
    val publishTime: String? = null,
    val thumbCount: Int = 0
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeInt(parentId)
        parcel.writeString(author)
        parcel.writeString(title)
        parcel.writeString(content)
        parcel.writeString(community)
        parcel.writeString(publishTime)
        parcel.writeInt(thumbCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Comment> {
        override fun createFromParcel(parcel: Parcel): Comment {
            return Comment(parcel)
        }

        override fun newArray(size: Int): Array<Comment?> {
            return arrayOfNulls(size)
        }
    }

}