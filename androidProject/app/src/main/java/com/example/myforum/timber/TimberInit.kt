package com.example.myforum.timber

import com.example.myforum.BuildConfig
import timber.log.Timber
import timber.log.Timber.DebugTree


object TimberInit {

    fun initTimer() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

}

private class CrashReportingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {}
}