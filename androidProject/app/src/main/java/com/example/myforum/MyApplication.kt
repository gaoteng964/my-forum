package com.example.myforum

import android.app.Application
import com.example.myforum.preference.MySharedPreference
import com.example.myforum.timber.TimberInit

class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        TimberInit.initTimer()
        MySharedPreference.initMySharedPreference(this.applicationContext)
    }

}