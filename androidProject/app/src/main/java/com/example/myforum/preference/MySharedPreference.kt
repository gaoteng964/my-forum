package com.example.myforum.preference

import android.content.Context
import com.example.myforum.data.RegisterUser
import timber.log.Timber

const val SHARED_PREFERENCE_FILE_NAME = "user_info"

object MySharedPreference {

    private lateinit var context: Context

    fun initMySharedPreference(context: Context) {
        this.context = context
    }

    fun saveUserInfo(user: RegisterUser) {
        Timber.i(" $user")
        this.context.getSharedPreferences(SHARED_PREFERENCE_FILE_NAME, Context.MODE_PRIVATE).edit()
            .run {
                this.putString("name", user.name)
                this.putString("password", user.password)
                this.putInt("age", user.age)
                apply()
            }
    }

    fun deleteUserInfo() {
        Timber.i(" ")
        this.context.getSharedPreferences(SHARED_PREFERENCE_FILE_NAME, Context.MODE_PRIVATE).edit()
            .run {
                this.remove("name")
                this.remove("password")
                this.remove("age")
                apply()
            }
    }

    fun getUserInfo(): RegisterUser? {
        Timber.i(" ")
        return this.context.getSharedPreferences(SHARED_PREFERENCE_FILE_NAME, Context.MODE_PRIVATE)
            .takeIf {
                it.getString("name", null) != null && it.getString("password", null) != null
            }?.run {
            RegisterUser(
                name = this.getString("name", "")!!,
                password = this.getString("password", "")!!,
                age = this.getInt("age", 0)
            )
        } ?: null
    }

}