package com.example.myforum.view

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.myforum.R
import timber.log.Timber

class CustomToolbar(context: Context, attrs: AttributeSet) :LinearLayout(context, attrs) {

    private var title: String = ""
    private var isShowBackButton: Boolean = true

    init {
        Timber.i("init")
        LayoutInflater.from(context).inflate(R.layout.custom_toolbar,this)
        context.obtainStyledAttributes(attrs, R.styleable.CustomToolbar).apply {
            title = this.getString(R.styleable.CustomToolbar_title) ?: ""
            isShowBackButton = this.getBoolean(R.styleable.CustomToolbar_isShowBackButton, false)
        }.recycle()
        findViewById<ImageView>(R.id.ivBack_toolbar).apply {
            visibility = if(isShowBackButton ) View.VISIBLE else View.GONE
        }.setOnClickListener {
            (context as Activity).finish()
        }
        findViewById<TextView>(R.id.tvTitle_toolbar).apply {
           text = title
        }
    }

}