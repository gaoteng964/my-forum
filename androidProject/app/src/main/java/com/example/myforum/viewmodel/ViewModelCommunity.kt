package com.example.myforum.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myforum.data.Community
import com.example.myforum.http.HttpConnection
import com.example.myforum.preference.MySharedPreference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModelCommunity: ViewModel() {

    val communityList = MutableLiveData<List<Community>>()

    fun refreshCommunityList() {
        viewModelScope.launch(Dispatchers.IO) {
            MySharedPreference.getUserInfo()?.run {
                HttpConnection.communityQuery()
            }?.run {
                communityList.postValue(this)
            } ?: communityList.postValue(emptyList())
        }
    }

}