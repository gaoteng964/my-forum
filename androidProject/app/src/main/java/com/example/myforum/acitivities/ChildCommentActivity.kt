package com.example.myforum.acitivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myforum.R
import com.example.myforum.adapter.AdapterChildComment
import com.example.myforum.data.Comment
import com.example.myforum.view.MySwipeRecycleView
import com.example.myforum.viewmodel.ViewModelMainActivity
import com.yanzhenjie.recyclerview.SwipeMenuBridge
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber

class ChildCommentActivity : AppCompatActivity() {

    private var fatherComment: Comment? = null
    private lateinit var viewModelMainActivity: ViewModelMainActivity
    private val adapterChildComment: AdapterChildComment = AdapterChildComment()
    private val deleteCommentFlow: MutableSharedFlow<Int?> = MutableStateFlow(null)
    private val thumbCountFlow: MutableSharedFlow<Comment?> = MutableStateFlow(null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_child_comment)
        viewModelMainActivity = ViewModelProvider(this)[ViewModelMainActivity::class.java]
        viewModelMainActivity.pullChildComments(fatherComment?.id)
        fatherComment = intent.extras?.get("currentComment") as Comment
        Timber.i(" $fatherComment")
        initFatherComment()
        initDeleteCommentFlow()
        initRecycleView()
        observeChildComments()
        initThumbCountFlow()
        lifecycleScope.launch(Dispatchers.IO) {
            viewModelMainActivity?.pullChildComments(fatherComment?.id)
        }
    }

    private fun initFatherComment(){
        findViewById<TextView>(R.id.tvCommentContent).text = fatherComment?.content
        findViewById<TextView>(R.id.tvCommentAuthor).text = fatherComment?.author
        findViewById<TextView>(R.id.tvPublishDate).text = fatherComment?.publishTime
    }

    private fun initRecycleView(){
        findViewById<MySwipeRecycleView>(R.id.rvComment).apply {
            layoutManager = LinearLayoutManager(context)
            initSwipeMenuCreator()
            setOnItemMenuClickListener {
                    menuBridge: SwipeMenuBridge, position: Int ->
                menuBridge.closeMenu()
                deleteCommentFlow.tryEmit(position)
            }
            adapter = adapterChildComment
        }
        registerThumbCount()
    }

    private fun initDeleteCommentFlow() {
        deleteCommentFlow.onEach {
            it ?: return@onEach
            val comment = adapterChildComment.getCommentList()[it]
            viewModelMainActivity?.deleteComment(comment)
            viewModelMainActivity?.pullChildComments(fatherComment?.id)
        }.flowOn(Dispatchers.IO).launchIn(lifecycleScope)
    }

    private fun observeChildComments() {
        viewModelMainActivity.childComments.observe(this) {
            adapterChildComment.setCommentList(it)
        }
    }

    private fun registerThumbCount() {
        adapterChildComment.setThumbCommentCallBack {
            thumbCountFlow.tryEmit(it)
        }
    }

    private fun initThumbCountFlow() {
        thumbCountFlow.onEach {
            it ?: return@onEach
            val isSuccessful = viewModelMainActivity?.thumbComment(it) ?: false
            viewModelMainActivity?.updateChildCommentList(isSuccessful, it)
        }.flowOn(Dispatchers.IO).launchIn(lifecycleScope)
    }

}