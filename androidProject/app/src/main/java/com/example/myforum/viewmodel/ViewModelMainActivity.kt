package com.example.myforum.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.example.myforum.data.Comment
import com.example.myforum.http.HttpConnection
import com.example.myforum.preference.MySharedPreference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModelMainActivity : ViewModel() {

    val commentsAllFather = MutableLiveData<List<Comment>>()
    val childComments = MutableLiveData<List<Comment>>()

    fun commentQueryAllFather() {
        viewModelScope.launch(Dispatchers.IO) {
            MySharedPreference.getUserInfo().run {
                HttpConnection.commentQueryAllFather()
            }?.run {
                commentsAllFather.postValue(this)
            } ?: commentsAllFather.postValue(emptyList())
        }
    }

    fun pullChildComments(parentCommentId: Int?) {
        parentCommentId ?: return
        MySharedPreference.getUserInfo().run {
            HttpConnection.commentQuerySub(parentCommentId)
        }?.run {
            childComments.postValue(this)
        } ?: childComments.postValue(emptyList())
    }

    fun thumbComment(comment: Comment): Boolean {
        return MySharedPreference.getUserInfo().run {
            comment.id?.let { HttpConnection.commentThumb(it, comment.thumbCount) }
        } ?: false
    }

    fun deleteComment(comment: Comment): Boolean {
        return MySharedPreference.getUserInfo().run {
            comment.id?.let { HttpConnection.commentDelete(it) }
        } ?: false
    }

    fun updateChildCommentList(isThumbSuccess: Boolean, comment: Comment ) {
        if(isThumbSuccess){
            comment.copy(thumbCount = comment.thumbCount + 1)
        }else {
            comment
        }.run {
            val thumbComment = this
            childComments.value?.map {
                if(thumbComment.id == it.id){
                    thumbComment
                }else{
                    it
                }
            }
        }.run {
            childComments.postValue(this)
        }
    }

    fun updateCommentList(isThumbSuccess: Boolean, comment: Comment ) {
        if(isThumbSuccess){
            comment.copy(thumbCount = comment.thumbCount + 1)
        }else {
            comment
        }.run {
            val thumbComment = this
            commentsAllFather.value?.map {
                if(thumbComment.id == it.id){
                    thumbComment
                }else{
                    it
                }
            }
        }.run {
            commentsAllFather.postValue(this)
        }
    }

}