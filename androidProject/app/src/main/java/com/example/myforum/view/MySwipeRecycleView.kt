package com.example.myforum.view

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import com.example.myforum.R
import com.example.myforum.adapter.AdapterHomeFragment
import com.example.myforum.adapter.AdapterHomeFragmentComment
import com.example.myforum.adapter.InterfaceForAdapterWithCommentList
import com.example.myforum.preference.MySharedPreference
import com.yanzhenjie.recyclerview.SwipeMenuCreator
import com.yanzhenjie.recyclerview.SwipeMenuItem
import com.yanzhenjie.recyclerview.SwipeRecyclerView

class MySwipeRecycleView(context: Context, attrs: AttributeSet) : SwipeRecyclerView(context, attrs ) {

    private val mSwipeMenuCreator =
        SwipeMenuCreator { _, rightMenu, position ->
            val deleteItem = SwipeMenuItem(context)
            val userInfo = MySharedPreference.getUserInfo()

            val currentComment = (originAdapter as InterfaceForAdapterWithCommentList).getCommentList()[position]
            if (userInfo?.name == currentComment.author) {
                deleteItem.setBackgroundColor(resources.getColor(R.color.purple_200))
                deleteItem.setText(R.string.delete_button_label)
                deleteItem.width =
                    context?.resources?.getDimension(R.dimen.recycle_item_delete_button)
                        ?.toInt() ?: 100
                deleteItem.height = ViewGroup.LayoutParams.MATCH_PARENT
                rightMenu?.addMenuItem(deleteItem)
            }
        }

    fun initSwipeMenuCreator(){
        setSwipeMenuCreator(mSwipeMenuCreator)
    }
}