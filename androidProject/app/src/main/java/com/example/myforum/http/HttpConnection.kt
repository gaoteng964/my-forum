package com.example.myforum.http

import android.util.Log
import com.example.myforum.data.Comment
import com.example.myforum.data.Community
import com.example.myforum.data.RegisterUser
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject

const val SERVER_URL = "http://182.43.143.233:8084"

enum class ActionType(val value : String) {
        UserAction(value =  "/myForm/user.action"),
        CommentAction(value = "/myForm/comment.action"),
        CommunityAction(value = "/myForm/community.action")
}

enum class OperationType(val value: String) {
    Login(value = "login"),
    Register(value = "register"),
    CommentAdd(value = "commentAdd"),
    CommentDelete(value= "commentDelete"),
    CommentThumb(value = "commentThumb"),
    CommentQueryAllFather(value = "commentQueryAllFather"),
    CommentQuerySub(value = "commentQuerySub"),
    CommentQueryMine(value = "commentQueryMine"),
    CommunityAdd(value = "communityAdd"),
    CommunityQuery(value = "communityQuery")
}

enum class StatusCode(val value: Int) {
    ERROR(value = 1),
    SUCCESS(value = 0)
}

object HttpConnection {

    val client = OkHttpClient()

    fun login(user: RegisterUser): Boolean {

        val url  = "$SERVER_URL${ActionType.UserAction.value}?operationType=${OperationType.Login.value}&" +
                "name=${user.name}&password=${user.password}"
        executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return false
            }
            return true
        }
    }

    fun register(user: RegisterUser): Boolean {
        val url  = "$SERVER_URL${ActionType.UserAction.value}?operationType=${OperationType.Register.value}&" +
                "name=${user.name}&password=${user.password}&age=${user.age}"
        executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return false
            }
            return true
        }
    }

    fun commentAdd(comment: Comment): Boolean {
        val url  = "$SERVER_URL${ActionType.CommentAction.value}?" +
                "operationType=${OperationType.CommentAdd.value}&parentId=${comment.parentId}&" +
                "author=${comment.author}&title=${comment.title}&content=${comment.content}&" +
                "community=${comment.community}&thumbCount=${comment.thumbCount}"
        executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return false
            }
            return true
        }
    }

    fun commentThumb(id: Int, thumbCount: Int): Boolean {
        val url  = "$SERVER_URL${ActionType.CommentAction.value}?" +
                "operationType=${OperationType.CommentThumb.value}&id=$id&thumbCount=${thumbCount + 1}"
        executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return false
            }
            return true
        }
    }

    fun commentDelete(id: Int): Boolean {
        val url  = "$SERVER_URL${ActionType.CommentAction.value}?" +
                "operationType=${OperationType.CommentDelete.value}&id=$id"
        executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return false
            }
            return true
        }
    }

    fun commentQueryAllFather(): List<Comment>? {
        val url  = "$SERVER_URL${ActionType.CommentAction.value}?" +
                "operationType=${OperationType.CommentQueryAllFather.value}"
        return executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return null
            }
            this.getJSONArray("result")
        }.run {
            processComment(this)
        }
    }

    fun commentQuerySub(parentId: Int): List<Comment>? {
        val url  = "$SERVER_URL${ActionType.CommentAction.value}?" +
                "operationType=${OperationType.CommentQuerySub.value}&parentId=${parentId}"
        return executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return null
            }
            this.getJSONArray("result")
        }.run {
            processComment(this)
        }
    }

    fun commentThumb(parentId: Int): List<Comment>? {
        val url  = "$SERVER_URL${ActionType.CommentAction.value}?" +
                "operationType=${OperationType.CommentQuerySub.value}&parentId=${parentId}"
        return executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return null
            }
            this.getJSONArray("result")
        }.run {
            processComment(this)
        }
    }

    fun commentQueryMine(userName: String): List<Comment>? {
        val url  = "$SERVER_URL${ActionType.CommentAction.value}?" +
                "operationType=${OperationType.CommentQueryMine.value}&author=${userName}"
        return executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return null
            }
            this.getJSONArray("result")
        }.run {
            processComment(this)
        }
    }

    fun communityAdd(community: Community): Boolean{
        val url  = "$SERVER_URL${ActionType.CommunityAction.value}?operationType=${OperationType.CommunityAdd.value}" +
                "&communityName=${community.communityName}&communityDescription=${community.communityDescription}&author=${community.author}"
        executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return false
            }
            return true
        }
    }

    fun communityQuery(): List<Community>?{
        val url  = "$SERVER_URL${ActionType.CommunityAction.value}?" +
                "operationType=${OperationType.CommunityQuery.value}"
        return executeUrl(url).run {
            val status = this.getInt("status")
            if(status == StatusCode.ERROR.value){
                return null
            }
            this.getJSONArray("result")
        }.run {
             processCommunity(this)
        }
    }

    private fun executeUrl(url : String): JSONObject {
        Log.i("executeUrl", url)
        return Request.Builder().url(url).build().run {
            client.newCall(this).execute()
        }.run {
            JSONObject(this.body?.string())
        }
    }

    private fun processComment(response: JSONArray): List<Comment>? {
        return (0 until response.length()).map {
            response.getJSONObject(it).run {
                Comment(
                    id = this.getInt("id"),
                    parentId = this.getInt("parentId"),
                    title = this.getString("title"),
                    content = this.getString("content"),
                    author = this.getString("author"),
                    thumbCount = this.getInt("thumbCount"),
                    publishTime = this.getString("publishTime"),
                    community = this.getString("community")
                )
            }

        }
    }

    private fun processCommunity(response: JSONArray): List<Community>? {
        return (0 until response.length()).map {
            response.getJSONObject(it).run {
                Community(
                    id = this.getInt("id"),
                    author = this.getString("author"),
                    communityName = this.getString("communityName"),
                    communityDescription = this.getString("communityDescription"),
                    publishTime = this.getString("publishTime")
                )
            }
        }
    }
}