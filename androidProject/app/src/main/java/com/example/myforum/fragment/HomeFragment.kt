package com.example.myforum.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myforum.R
import com.example.myforum.acitivities.ChildCommentActivity
import com.example.myforum.adapter.AdapterHomeFragmentComment
import com.example.myforum.data.Comment
import com.example.myforum.data.RegisterUser
import com.example.myforum.preference.MySharedPreference
import com.example.myforum.view.MySwipeRecycleView
import com.example.myforum.viewmodel.ViewModelMainActivity
import com.yanzhenjie.recyclerview.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var adapterHomeFragmentComment = AdapterHomeFragmentComment(
        emptyList()
    )
    private var viewModelMainActivity: ViewModelMainActivity? = null

    private  var mySwipeRecyclerView : MySwipeRecycleView? = null

    private val thumbCountFlow: MutableSharedFlow<Comment?> = MutableStateFlow(null)

    private val deleteCommentFlow: MutableSharedFlow<Int?> = MutableStateFlow(null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initThumbCountFlow()
        initDeleteCommentFlow()
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onResume() {
        super.onResume()
        viewModelMainActivity?.commentQueryAllFather()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModelMainActivity = ViewModelProvider(this)[ViewModelMainActivity::class.java]
        viewModelMainActivity?.commentQueryAllFather()
        viewModelMainActivity?.commentsAllFather?.observe(viewLifecycleOwner) {
            adapterHomeFragmentComment.setCommentList(it)
        }
        initRecycleView(view)
    }

    private fun initRecycleView(view: View) {
        mySwipeRecyclerView = view.findViewById<MySwipeRecycleView>(R.id.rvComment).apply {
            layoutManager = LinearLayoutManager(context)
            initSwipeMenuCreator()
            setOnItemMenuClickListener {
                menuBridge: SwipeMenuBridge, position: Int ->
                menuBridge.closeMenu()
                deleteCommentFlow.tryEmit(position)
            }
            adapter = adapterHomeFragmentComment
        }
        registerThumbCount()
        registerItemClickCallBack()
    }

    private fun registerItemClickCallBack() {
        adapterHomeFragmentComment.setItemClickCallBack { comment ->
            Intent(context, ChildCommentActivity::class.java).apply {
                putExtra("currentComment", comment)
            }.run {
                startActivity(this)
            }
        }
    }

    private fun registerThumbCount() {
        adapterHomeFragmentComment.setThumbCommentCallBack {
            thumbCountFlow.tryEmit(it)
        }
    }

    private fun initDeleteCommentFlow() {
        deleteCommentFlow.onEach {
            it ?: return@onEach
            val comment = adapterHomeFragmentComment.getCommentList()[it]
            viewModelMainActivity?.deleteComment(comment)
            viewModelMainActivity?.commentQueryAllFather()
        }.flowOn(Dispatchers.IO).launchIn(lifecycleScope)
    }

    private fun initThumbCountFlow() {
        thumbCountFlow.onEach {
            it ?: return@onEach
            val isSuccessful = viewModelMainActivity?.thumbComment(it) ?: false
            viewModelMainActivity?.updateCommentList(isSuccessful, it)
        }.flowOn(Dispatchers.IO).launchIn(lifecycleScope)
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}