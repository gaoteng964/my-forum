package com.example.myforum.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myforum.R
import com.example.myforum.data.Comment

class AdapterHomeFragmentComment(commentList: List<Comment>): RecyclerView.Adapter<RecyclerView.ViewHolder>() ,InterfaceForAdapterWithCommentList{

    private var currentComments: ArrayList<Comment> = ArrayList(commentList)
    private lateinit var thumbCommentCallBack: (comment: Comment) -> Unit
    private lateinit var itemClickCallBack : (comment: Comment) -> Unit

    inner class CommentViewHolder(private val view: View):RecyclerView.ViewHolder(view)  {
        val tvCommentTitle = view.findViewById<TextView>(R.id.tvCommentTitle)
        val tvCommentAuthor =  view.findViewById<TextView>(R.id.tvCommentAuthor)
        val tvCommunity =  view.findViewById<TextView>(R.id.tvCommunity)
        val tvThumcount = view.findViewById<TextView>(R.id.tvThumcount)
        val tvContent = view.findViewById<TextView>(R.id.tvContent)
        val ivThumbcount = view.findViewById<ImageView>(R.id.ivThumbcount)

        fun bindData(comment: Comment){
            tvCommentTitle.text = comment.title
            tvCommentAuthor.text = comment.author
            tvCommunity.text = comment.community
            tvThumcount.text = comment.thumbCount.toString()
            tvContent.text = comment.content
            ivThumbcount.setOnClickListener {
                thumbCommentCallBack.invoke(comment)
            }
            tvThumcount.setOnClickListener {
                thumbCommentCallBack.invoke(comment)
            }
            view.setOnClickListener {
                itemClickCallBack.invoke(comment)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return LayoutInflater.from(parent.context).inflate(R.layout.recyle_item_home_fragment_comment, parent, false).run {
            CommentViewHolder(this)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CommentViewHolder).bindData(currentComments[position])
    }

    override fun getItemCount(): Int = currentComments.size

    fun setCommentList(comments: List<Comment>){
        currentComments.clear()
        currentComments.addAll(comments)
        notifyDataSetChanged()
    }

    override fun getCommentList(): List<Comment>{
        return currentComments
    }

    fun setThumbCommentCallBack(callBack: (comment: Comment) -> Unit) {
        this.thumbCommentCallBack = callBack
    }

    fun setItemClickCallBack(callBack: (comment: Comment) -> Unit) {
        this.itemClickCallBack = callBack
    }

}