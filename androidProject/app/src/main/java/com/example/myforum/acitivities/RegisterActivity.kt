package com.example.myforum.acitivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.myforum.MainActivity
import com.example.myforum.R
import com.example.myforum.data.RegisterUser
import com.example.myforum.http.HttpConnection
import com.example.myforum.preference.MySharedPreference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*

class RegisterActivity : AppCompatActivity() {

    val registerFlow: MutableStateFlow<RegisterUser?> = MutableStateFlow(null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initLoginFlow()
        registerLoginClickEvent()
    }


    private fun registerLoginClickEvent() {
        findViewById<Button>(R.id.btnRegister).setOnClickListener {
            val name = findViewById<EditText>(R.id.etName).text.toString()
            val password = findViewById<EditText>(R.id.etPassword).text.toString()
            val age = findViewById<EditText>(R.id.etAge).text.toString()
            if(name.isNullOrBlank()){
                Toast.makeText(this@RegisterActivity.baseContext, R.string.login_activity_name_cant_be_blank_or_empty, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if(password.isNullOrBlank()){
                Toast.makeText(this@RegisterActivity.baseContext, R.string.login_activity_password_cant_be_blank_or_empty, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(age.isNullOrBlank()){
                Toast.makeText(this@RegisterActivity.baseContext, R.string.login_activity_Age_cant_be_blank_or_empty, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            registerFlow.tryEmit(RegisterUser(name = name, password = password, age = age.toInt()))
        }
    }

    private fun initLoginFlow() {
        registerFlow.map {
            it ?: return@map false
            val isSuccess = HttpConnection.register(it)
            if(isSuccess){
                MySharedPreference.saveUserInfo(it)
            }
            isSuccess
        }.flowOn(Dispatchers.IO).onEach {

            if(it){
                Intent(this@RegisterActivity, MainActivity::class.java).run {
                    startActivity(this)
                }
            }else {
                Toast.makeText(this@RegisterActivity.baseContext, R.string.fail_to_register, Toast.LENGTH_SHORT).show()
            }
        }.flowOn(Dispatchers.Main).launchIn(lifecycleScope)
    }
}