const express = require('express')
const { connection } = require("./dbConnection/DbConnection")
const app = express()
const port = 8083

let mysqlConnectionPromise = null
app.use(express.json())
app.use(express.urlencoded({extended: true}))

function createMysqlConnectionPromise() {
    if(mysqlConnectionPromise != null){
        return mysqlConnectionPromise
    }
    mysqlConnectionPromise = new Promise(function(resolve, reject) {
        connection.connect(function(err) {
            if(err){
                mysqlConnectionPromise = null
                reject(`${err}`)
                return
            }
            resolve()
        })
    })
    return mysqlConnectionPromise
}

function createExecuteSqlPromise(sql, params){
    return new Promise(function(resolve, reject) {
        connection.query(sql, params, function(error, result){
            if(error){
                console.log(`${error}`)
                reject(`${error}`)
                return
            }
            console.log(`${result}`)
            resolve(result)
        })
    })
}

// login register. commentAdd commentDelete commentQueryAllFather, commentQuerySub commentQueryMine communityAdd

app.get('/myForm/user.action', (req, res) => {
    const operationType = req.query.operationType
    const id = req.query.id
    const name = req.query.name
    const password = req.query.password
    const age = req.query.age

    const sqlLogin = "SELECT * FROM RegisterUsers WHERE name = ? AND password = ? "
    const sqlRegister = "INSERT INTO RegisterUsers (name, password, age) VALUES (?, ?, ?)"

    const loginParams = [name, password]
    const registerParams = [name, password, age]

    if(operationType == "login"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlLogin,  loginParams)
            })
            .then(function(result) {
                if(result.length > 0){
                    res.send({
                        status: 0,
                        message: "login successfully"
                    })
                }else {
                    return Promise.reject("the username and password does not match or there is no such user")
                }
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error}`
                })
            })
    }else if(operationType == "register"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlRegister,  registerParams)
            })
            .then(function(result) {
                console.log(result)
                if(result.affectedRows == 1){
                    res.send({
                        status: 0,
                        message: "register successfully"
                    })
                }else {
                    return Promise.reject("can not insert such user")
                }
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error}`
                })
            })
    } else {
        res.send({
            status: 1,
            message: `no such operation`
        })
    }
})


// login register. commentAdd commentDelete commentQueryAllFather, commentQuerySub commentQueryMine communityAdd

app.get('/myForm/comment.action', (req, res) => {
//http://localhost:8084/myForm/comment.action?operationType=commentAdd&parentId=-1&author=gaoteng&title=dddd&content=ffffffff&community=%E5%88%9B%E4%B8%9A%E6%9D%BF&thumbCount=9
    const operationType = req.query.operationType
    const id = req.query.id
    const parentId = req.query.parentId
    const author = req.query.author
    const title = req.query.title
    const content = req.query.content
    const community = req.query.community
    const thumbCount = req.query.thumbCount

    const sqlCommentAdd = "INSERT INTO Comment (parentId, author, title, content, community, publishTime, thumbCount) VALUES (?, ? , ? , ?, ? ,NOW(), ?)"
    const paramsCommentAdd = [parentId, author, title, content, community, thumbCount]

    const sqlCommentDelete = "DELETE FROM Comment WHERE id = ? OR parentId = ?"
    const paramsCommentDelete = [id, id]

    const sqlCommentQueryAllFather = "SELECT * FROM Comment WHERE parentId = -1"

    const sqlCommentQuerySub = "SELECT * FROM Comment WHERE parentId = ?"
    const paramsCommentQuerySub = [parentId]

    const sqlCommentQueryMine = "SELECT * FROM Comment WHERE author = ?"
    const paramsCommentQueryMine = [author]

    const sqlCommentThumb = "UPDATE Comment SET thumbCount = ? WHERE id = ?"
    const paramsCommentThumb = [thumbCount, id]

    if(operationType == "commentAdd"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlCommentAdd,  paramsCommentAdd)
            })
            .then(function(result) {
                console.log(result)
                if(result.affectedRows == 1){
                    res.send({
                        status: 0,
                        message: "comment add successfully"
                    })
                }
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error}`
                })
            })
    } else if(operationType == "commentThumb"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlCommentThumb,  paramsCommentThumb)
            })
            .then(function(result) {
                console.log(result)
                if(result.affectedRows == 1){
                    res.send({
                        status: 0,
                        message: "comment thumb successfully"
                    })
                }
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error}`
                })
            })
    }else if(operationType == "commentDelete"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlCommentDelete,  paramsCommentDelete)
            })
            .then(function(result) {
                console.log(result)
                if(result.affectedRows == 1){
                    res.send({
                        status: 0,
                        message: "comment delete successfully"
                    })
                }
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error}`
                })
            })
    } else if(operationType == "commentQueryAllFather"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlCommentQueryAllFather,  [])
            })
            .then(function(result) {
                console.log(result)
                res.send({
                    status: 0,
                    result: result
                })
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error}`
                })
            })
    }else if(operationType == "commentQuerySub"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlCommentQuerySub,  paramsCommentQuerySub)
            })
            .then(function(result) {
                console.log(result)
                res.send({
                    status: 0,
                    result: result
                })
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error}`
                })
            })
    } else if(operationType == "commentQueryMine"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlCommentQueryMine,  paramsCommentQueryMine)
            })
            .then(function(result) {
                console.log(result)
                res.send({
                    status: 0,
                    result: result
                })
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error}`
                })
            })
    } else {
        res.send({
            status: 1,
            message: `no such operation`
        })
    }
})

// login register. commentAdd commentDelete commentQueryAllFather, commentQuerySub commentQueryMine communityAdd

app.get('/myForm/community.action', (req, res) => {

    const operationType = req.query.operationType
    const communityName = req.query.communityName
    const communityDescription = req.query.communityDescription
    const author = req.query.author
    console.log(communityName)
    console.log(communityDescription)
    console.log(author)
    const sqlCommunityAdd = "INSERT INTO Community (communityName , communityDescription, publishTime,author) VALUES (?, ? , NOW(),?)"
    const paramsCommunityAdd = [communityName, communityDescription, author]

    const sqlCommunityQuery = "SELECT * FROM Community"

    if(operationType == "communityAdd"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlCommunityAdd,  paramsCommunityAdd)
            })
            .then(function(result) {
                console.log(result)
                if(result.affectedRows == 1){
                    res.send({
                        status: 0,
                        message: "community add successfully"
                    })
                }
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error} tEST`
                })
            })
    }else if(operationType == "communityQuery"){
        createMysqlConnectionPromise()
            .then(function(){
                return createExecuteSqlPromise(sqlCommunityQuery,  [])
            })
            .then(function(result) {
                console.log(result)
                res.send({
                    status: 0,
                    result:result
                })
            })
            .catch(function(error) {
                res.send({
                    status: 1,
                    message: `${error}`
                })
            })
    } else  {
        res.send({
            status: 1,
            message: `no such operation`
        })
    }
})

// to keep connection alive to the database
setInterval(function() {
    const sqlCommunityQuery = "SELECT * FROM Community"
    createMysqlConnectionPromise()
        .then(function(){
            return createExecuteSqlPromise(sqlCommunityQuery,  [])
        })
        .then(function(result) {
            console.log(result)
            console.log("keep alive successfully")
        })
        .catch(function(error){
            console.log(error)
            console.log("keep alive failed")
        })
}, 60 * 1000)


app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})